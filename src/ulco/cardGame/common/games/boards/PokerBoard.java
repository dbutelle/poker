package ulco.cardGame.common.games.boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PokerBoard implements Board {
    private List<Card> cards;
    private List<Coin> coins;

    /**
     * Constructeur
     */
    public PokerBoard() {
        this.cards = new ArrayList<>();
        this.coins = new ArrayList<>();
    }

    /**
     * Vidage des listes
     */
    public void clear() {
        this.cards.clear();
        this.coins.clear();
    }

    /**
     * Ajoute en fonction du component
     * @param component
     */
    public void addComponent(Component component) {
        if (component.getClass() == Card.class) {
            this.cards.add((Card) component);
        } else if (component.getClass() == Coin.class) {
            this.coins.add((Coin) component);
        }
    }

    public List<Component> getComponents() {
        List<Component> components = new ArrayList<>(cards);
        components.addAll(coins);
        return components;
    }

    /**
     *
     * @param classType
     * @return liste de carte
     * @return liste de coins
     */
    public List<Component> getSpecificComponents(Class classType) {
        if (classType == Card.class) {
            return new ArrayList<>(cards);
        } else if (classType == Coin.class) {
            return new ArrayList<>(coins);
        }
        return null;
    }

    /**
     * Affichage des stats
     */
    public void displayState() {
        int pot = 0;
        System.out.println("---------- Board State ----------");
        System.out.println("--------");
        //Affichage des cartes si elle ne sont pas cachée
        for (Card card : cards) {
            if (card.isHidden() == false) {
                System.out.println("Card : "+card.getName());
            }
        }
        System.out.println("--------");
        //Hashmap pour tri des jetons
        HashMap<String, Integer> triJetons = new HashMap<>();
        //Pour chaque jetons
        for (Coin jetons : coins) {
            //ajout de la valeur du jeton dans le pot
            pot += jetons.getValue();
            if (triJetons.containsKey(jetons.getName())==false) {
                triJetons.put(jetons.getName(), 1);
            } else {
                triJetons.replace(jetons.getName(), triJetons.get(jetons.getName()) + 1);
            }
        }
        //Affichage des jetons
        for (Map.Entry<String, Integer> entry : triJetons.entrySet()) {
            String couleurJeton = entry.getKey();
            Integer nbrJetons = entry.getValue();
            System.out.println("- Coin " + couleurJeton + " x " + nbrJetons);
        }

        System.out.println("Your Coins sum placed is about " + pot);
        System.out.println("-------------------------------");
    }

}
