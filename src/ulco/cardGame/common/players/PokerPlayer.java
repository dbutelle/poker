package ulco.cardGame.common.players;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.*;

public class PokerPlayer extends BoardPlayer implements Player {
    private List<Card> cards;
    private List<Coin> coins;

    /**
     * Constructeur
     * @param name
     */
    public PokerPlayer(String name) {
        super(name);
        cards = new ArrayList<>();
        coins = new ArrayList<>();
    }

    /**
     *  Récupération du score
     * @return score
     */
    public Integer getScore() {
        Integer score = 0;
        for (Coin coin : coins) {
            score += coin.getValue();
        }
        return score;
    }

    /**
     * methode de récupération du jeton joué
     * @return le jeton
     */
    public void play(Socket socket) throws IOException {
        ObjectOutputStream sortie = new ObjectOutputStream(socket.getOutputStream());
        boolean good = true;
        while (true) {
            System.out.println("Fold or bet ?");
            Scanner scanner = new Scanner(System.in);
            String bof = scanner.next();
            //vérification de ce qu'il rentre
            while (bof.compareTo("fold")!=0 && bof.compareTo("bet")!=0) {
                System.out.println("Fold or bet ?");
                bof = scanner.next();
            }
            if(bof.compareTo("fold")==0){
                sortie.writeObject(null);
                return;
            }else{
                System.out.println(this.name + ", please select a valid Coin to play (coin color) ");
                String couleur = scanner.next();
                for (Coin jetons : coins) {
                    if (couleur.compareTo(jetons.getName())==0) {
                        coins.remove(jetons);
                        System.out.println("PPp "+jetons);
                        sortie.writeObject(jetons);
                        return;
                    }
                }
            }


        }
    }

    /**
     *  Ajout d'un component à sa liste
     * @param component
     */
    public void addComponent(Component component) {
        if (component instanceof Card) {
            cards.add((Card) component);
        } else if (component instanceof Coin) {
            coins.add((Coin) component);
            score = score + component.getValue();
        }
    }

    /**
     *  Remove un component de sa liste
     * @param component
     */
    public void removeComponent(Component component) {
        if (component instanceof Card) {
            cards.remove(component);
        } else if (component instanceof Coin) {

            Coin coinToRemove = null;
            score -= component.getValue();
            for (Coin coin : coins){
                if(coin.getName().compareTo(component.getName()) == 0){
                    coinToRemove = coin;
                    break;
                }
            }
            coins.remove(coinToRemove);
        }
    }


    /**
     * Récupération de la liste
     * @return
     */
    public List<Component> getComponents() {
        List<Component> components = new ArrayList<>(cards);
        components.addAll(coins);
        return components;
    }

    /**
     * Récup de la liste en fonction du type
     * @param classType
     * @return
     */
    public List<Component> getSpecificComponents(Class classType) {
        if (classType == Card.class) {
            return new ArrayList<>(cards);
        } else if (classType == Coin.class) {
            return new ArrayList<>(coins);
        }
        return null;
    }

    /**
     * Mélange du jeu
     */
    public void shuffleHand() {
        Collections.shuffle(cards);
    }

    /**
     * On clear les mains du joueur
     */
    public void clearHand() {
        cards.clear();
    }

    /**
     * Affichage de la main du joueur
     */
    public void displayHand() {
        System.out.println("-------------------------------");
        System.out.println("Hand of " + this.name);
        System.out.println("--------");
        for (Card card : cards) {
            System.out.println("Card : "+card.getName() + " ");
        }
        System.out.println("--------");


        HashMap<String, Integer> jetons = new HashMap<>();
        for (Coin coin : coins) {
            if (!jetons.containsKey(coin.getName())) {
                jetons.put(coin.getName(), 1);
            } else {
                jetons.replace(coin.getName(), jetons.get(coin.getName()) + 1);
            }
        }
        for (Map.Entry<String, Integer> jtn : jetons.entrySet()) {
            String couleurJeton = jtn.getKey();
            Integer nbrJetons = jtn.getValue();
            System.out.println("- Coin " + couleurJeton + " x " + nbrJetons);
        }
        System.out.println("Your Coins sum is about : " + getScore());
        System.out.println("-------------------------------");
    }

    /**
     * Hop le toString
     * @return
     */
    @Override
    public String toString() {
        return "PokerPlayer{" +
                "name='" + name + '\'' +
                ", playing=" + playing +
                ", score=" + score +
                '}';
    }
}
