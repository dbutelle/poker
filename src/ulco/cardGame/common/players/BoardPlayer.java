package ulco.cardGame.common.players;

import ulco.cardGame.common.interfaces.Player;

public abstract class BoardPlayer implements Player {
    protected String name;
    protected boolean playing;
    protected Integer score;

    /**
     * Constructeur qui initialise le nom, le score, et le fait que le joueur joue
     * @param name
     */
    public BoardPlayer(String name){

        this.name=name;
        this.score=0;
        this.playing=false;
    }

    /**
     * Setter pour jouer
     * @param playing
     */
    public void canPlay(boolean playing){
        this.playing=playing;
    }

    /**
     * Getter du name
     * @return name
     */
    public String getName(){
        return this.name;
    }

    /**
     * Getter de playing
     * @return playing
     */
    public boolean isPlaying(){
        return this.playing;
    }

    /**
     *
     * @return String
     */
    public String toString(){
        String msg= "Nom du joueur : "+this.name +"; Score : "+this.score;
        return msg;
    }

}
